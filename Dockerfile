FROM resin/raspberrypi-python:3.5-slim

COPY ./requirements.txt /
RUN pip install --no-cache-dir -r requirements.txt

ENV PHANT_PUBLIC_KEY 9WXYN49K1JTqv9V1mqP6SxyewvkQ
ENV PHANT_PRIVATE_KEY KZgGb2N3VlS5nr9DA5jQtQmvagD8
ENV PHANT_DELETE_KEY 1A8QPVR796iRdwrN7RQbF4yVLaAe

COPY ./watering.py /

ENTRYPOINT ["python"]
CMD ["watering.py"]

