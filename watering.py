import time
import os
import sys
import atexit

# Import GPIO, SPI (for hardware SPI) and MCP3008 libraries.
import RPi.GPIO as GPIO
import Adafruit_GPIO.SPI as SPI
import Adafruit_MCP3008

# Import Phant library
from phant import Phant

# Configure output pin to control measuring
PIN_OUT = 3
GPIO.setmode(GPIO.BOARD)
GPIO.setup(PIN_OUT, GPIO.OUT)
GPIO.output(PIN_OUT, GPIO.HIGH)
atexit.register(GPIO.cleanup)

# Time between each measurement in seconds
SLEEP_TIME = 60.0*5.0

# Configure hardware SPI
SPI_PORT   = 0
SPI_DEVICE = 0
mcp = Adafruit_MCP3008.MCP3008(spi=SPI.SpiDev(SPI_PORT, SPI_DEVICE))

# Setup phant
phant_url = 'http://192.168.1.249:8080'
try:
    public_key = os.environ['PHANT_PUBLIC_KEY']
    private_key = os.environ['PHANT_PRIVATE_KEY']
except KeyError:
    print("Could not find phant environment variables."
          "Need to set PHANT_PUBLIC_KEY and PHANT_PRIVATE_KEY environment variables")
    raise
p = Phant(public_key, 'moisture', base_url=phant_url, private_key=private_key)

# Main program loop.
GPIO_STATE = GPIO.HIGH
while True:
    GPIO.output(PIN_OUT, GPIO.HIGH)
    time.sleep(0.05)
    value = mcp.read_adc(0)
    p.log(value)
    time.sleep(0.01)
    GPIO.output(PIN_OUT, GPIO.LOW)
    
    print("{} logged".format(value))
    sys.stdout.flush()
    time.sleep(SLEEP_TIME)
