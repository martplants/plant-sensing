Things to do before starting script:

- Set the environment variables PHANT_PUBLIC_KEY and PHANT_PRIVATE_KEY in Dockerfile
- Make sure SPI interface is enabled on Raspberry Pi
- Make sure to set the correct url of the Phant server in watering.py